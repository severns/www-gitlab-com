# frozen_string_literal: true

require 'base64'
require 'cgi'
require 'open-uri'
require 'uri'

module ReleasePosts
  class Issue
    include Helpers

    PROJECT_PATH_REGEX = %r{/(?<project>.*?/.*?)/}
    PROBLEM_REGEX = /^[#]+ Problem to solve\W*^(?<description>.*?)\s*^#/im
    IMAGE_REGEX = /\!\[mockup\]\((?<image>.*?)\)/im

    def initialize(issue, type_name)
      @issue = issue
      @type_name = type_name

      # Gracefully handle web URL, and upgrade to the API response
      return if issue['_links']
      return unless matches = issue['web_url'].match(PROJECT_PATH_REGEX)

      api_url = "https://gitlab.com/api/v4/projects/#{CGI.escape(matches[:project])}/issues/#{issue['iid']}"
      body = URI.open(api_url, 'Accept' => 'application/json').read
      @issue = JSON.parse(body)
    end

    def iid
      @issue['iid']
    end

    def project_id
      @issue['project_id']
    end

    def title
      @issue['title']
    end

    def description
      @issue['description']
    end

    def labels
      @issue['labels']
    end

    def milestone_id
      @issue['milestone']['iid']
    end

    def web_url
      @issue['web_url']
    end

    def group_label
      labels.find { |label| label.start_with? GROUP_LABEL_PREFIX }
    end

    def group
      group_label.delete_prefix(GROUP_LABEL_PREFIX)
    end

    def stage_label
      labels.find { |label| label.start_with? STAGE_LABEL_PREFIX }
    end

    def stage
      stage_label.delete_prefix(STAGE_LABEL_PREFIX)
    end

    def categories
      category_labels = labels.select { |label| label.start_with? CATEGORY_LABEL_PREFIX }
      category_labels.map { |label| titleize(label.delete_prefix(CATEGORY_LABEL_PREFIX)) }
    end

    def available_in
      available_in = ['ultimate']
      return available_in if labels.include? 'GitLab Ultimate'

      available_in.unshift('premium')
      return available_in if labels.include? 'GitLab Premium'

      available_in.unshift('starter')
      return available_in if labels.include? 'GitLab Starter'

      available_in.unshift('core')
    end

    def type_name
      return @type_name if @type_name

      l = labels.find { |label| label.start_with? RP_LABEL_PREFIX }
      type_found = TYPES.find do |type|
        type.label == l
      end

      type_found ? type_found.name : nil
    end

    def slug
      slugify("#{stage}_#{group}_#{title}")
    end

    def problem_to_solve
      if matches = description.match(PROBLEM_REGEX)
        matches[:description]
      else
        "Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit. Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n"
      end
    end

    def image_url
      return unless matches = description.match(IMAGE_REGEX)

      matches[:image].to_s
    end

    def image_binary
      u = URI(web_url.split('/-/')[0])
      u.path += image_url

      URI.open(u).read
    end

    def image_base64
      Base64.strict_encode64(image_binary)
    end

    attr_reader :url

    private

    def titleize(string)
      string.split.each(&:capitalize!).join(' ').to_s
    end

    def slugify(value)
      value.strip.gsub(/\s/, '_').gsub(/\W/, '').downcase
    end
  end
end
