# frozen_string_literal: true

require 'open-uri'

require_relative '../stage'
require_relative './styled_yaml'
require_relative '../team'

module ReleasePosts
  class PostEntry
    include Helpers

    attr_reader :options

    def initialize(options)
      @options = options
      @issue = Issue.new(JSON.parse(URI.open(options.issue_url, 'Accept' => 'application/json').read), options.type)
    end

    def execute
      assert_feature_branch!
      assert_issue_url!
      assert_new_file!

      # Read type from $stdin unless it is already set
      @block_type = @issue.type_name || InputParser.read_type
      assert_valid_type!

      write_local
    end

    private

    def contents
      block_type = @block_type
      yml = case block_type
            when 'deprecation'
              deprecation_content
            when 'removal'
              removal_content
            when 'top', 'primary', 'secondary'
              feature_content(block_type)
            else
              fail_with "Unknown content block type '#{block_type}'"
            end

      remove_trailing_whitespace(yml)
    end

    def feature_content(type_name)
      StyledYAML.dump(
        'features' => {
          type_name => [
            {
              'name' => StyledYAML.double_quoted(@issue.title),
              'available_in' => StyledYAML.inline(@issue.available_in),
              'gitlab_com' => true,
              'documentation_link' => StyledYAML.single_quoted(+'https://docs.gitlab.com/ee/#amazing'),
              'image_url' => StyledYAML.single_quoted(image_path.delete_prefix('source/')),
              'reporter' => reporter,
              'stage' => @issue.stage,
              'categories' => @issue.categories,
              'issue_url' => StyledYAML.single_quoted(@issue.web_url),
              'description' => StyledYAML.literal(@issue.problem_to_solve)
            }
          ]
        }
      )
    end

    def deprecation_content
      StyledYAML.dump(
        'deprecations' => {
          'feature_name' => StyledYAML.double_quoted(@issue.title),
          'reporter' => reporter,
          'due' => 'April 22, 2019',
          'issue_url' => StyledYAML.single_quoted(@issue.url),
          'description' => StyledYAML.literal(@issue.problem_to_solve)
        }
      )
    end

    def removal_content
      StyledYAML.dump(
        'removals' => {
          'feature_name' => StyledYAML.double_quoted(@issue.title),
          'reporter' => reporter,
          'date_of_removal' => 'April 22, 2019',
          'issue_url' => StyledYAML.single_quoted(@issue.url),
          'description' => StyledYAML.literal(@issue.problem_to_solve)
        }
      )
    end

    def write_local
      abs_image_path = File.join(git_repo_path, image_path)
      abs_content_path = File.join(git_repo_path, file_path)

      $stdout.puts "\n\e[32mcreate\e[0m #{abs_image_path}"
      $stdout.puts "\n\e[32mcreate\e[0m #{abs_content_path}"
      $stdout.puts contents
      $stdout.puts "\n\e[34mhint\e[0m remember to stage and commit, before you push!"

      return if options.dry_run

      File.write(abs_file_path, contents)
      File.write(abs_image_path, @issue.image_binary) if @issue.image_url

      system("#{editor} '#{file_path}'") if editor
    end

    def editor
      ENV['EDITOR']
    end

    def assert_feature_branch!
      return unless branch_name == 'master'

      fail_with "Create a branch first!"
    end

    def assert_new_file!
      return unless File.exist?(file_path)
      return if options.force

      fail_with "#{file_path} already exists! Use `--force` to overwrite."
    end

    def assert_issue_url!
      return unless options.issue_url.empty?

      fail_with "Provide an issue URL for the release post item"
    end

    def assert_valid_type!
      return unless @block_type && @block_type == InputParser::INVALID_TYPE

      fail_with 'Invalid category given!'
    end

    def file_path
      ext = '.yml'
      base_path = File.join(unreleased_path, @issue.slug)
      +base_path[0..MAX_FILENAME_LENGTH - ext.length] + ext
    end

    def image_path
      return +'/images/unreleased/feature-a.png' unless @issue.image_url

      ext = File.extname(@issue.image_url)
      base_path = File.join(unreleased_images_path, @issue.slug)
      +base_path[0..MAX_FILENAME_LENGTH - ext.length] + ext
    end

    def unreleased_path
      File.join('data', 'release_posts', 'unreleased')
    end

    def unreleased_images_path
      File.join('source', 'images', 'unreleased')
    end

    def branch_name
      @branch_name ||= capture_stdout(%w[git symbolic-ref --short HEAD]).strip
    end

    def reporter
      username = ENV['GITLAB_USERNAME']
      return if username

      pm_name = Gitlab::Homepage::Stage.all!
        .find { |stage| stage.key == @issue.stage }
        .groups[@issue.group]['pm']
      Gitlab::Homepage::Team::Member.all!
        .find { |person| person.name == pm_name }
        .gitlab
    end

    def remove_trailing_whitespace(yaml_content)
      yaml_content.gsub(/ +$/, '')
    end
  end
end
