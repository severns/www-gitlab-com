# frozen_string_literal: true

module ReleasePosts
  class InputParser
    extend Helpers

    TYPES_OFFSET = 1
    INVALID_TYPE = -1

    Options = Struct.new(
      :author,
      :dry_run,
      :force,
      :issue_url,
      :type
    )

    class << self
      def parse(argv)
        options = Options.new

        parser = OptionParser.new do |opts|
          opts.banner = "Usage: #{__FILE__} [options] [issue url]\n\n"

          opts.on('-f', '--force', 'Overwrite an existing entry') do |value|
            options.force = value
          end

          opts.on('-n', '--dry-run', "Don't actually write anything, just print") do |value|
            options.dry_run = value
          end

          opts.on('-t', '--type [string]', String, "The category of the change, valid options are: #{TYPES.map(&:name).join(', ')}") do |value|
            options.type = parse_type(value)
          end

          opts.on('-h', '--help', 'Print help message') do
            $stdout.puts opts
            raise Done
          end
        end

        parser.parse!(argv)

        # Issue URL is everything that remians
        options.issue_url = argv.join(' ').strip.squeeze(' ').tr("\r\n", '')

        options
      end

      def read_type
        read_type_message

        type = TYPES[$stdin.gets.to_i - TYPES_OFFSET]
        assert_valid_type!(type)

        type.name
      end

      private

      def parse_type(name)
        type_found = TYPES.find do |type|
          type.name == name
        end
        type_found ? type_found.name : INVALID_TYPE
      end

      def read_type_message
        $stdout.puts "\n>> Please specify the index for the category of release post item:"
        TYPES.each_with_index do |type, index|
          $stdout.puts "#{index + TYPES_OFFSET}. #{type.description}"
        end
        $stdout.print "\n?> "
      end

      def assert_valid_type!(type)
        return if type

        raise Abort, "Invalid category index, please select an index between 1 and #{TYPES.length}"
      end
    end
  end
end
