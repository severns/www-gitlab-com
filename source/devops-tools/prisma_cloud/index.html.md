---
layout: markdown_page
title: "Prisma Cloud"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab-Prisma Cloud Comparison Infographic
  This summary infographic compares Prisma Cloud and GitLab across several DevOps Stages and Categories.

![GitLab Prisma Cloud Comparison Chart](/devops-tools/prisma_cloud/GitLab_PrismaCloud.jpg)

## Summary

Twistlock was recently [acquired](https://www.paloaltonetworks.com/company/press/2019/palo-alto-networks-completes-acquisition-of-twistlock) by Palo Alto and subsequently rebranded as Prisma Cloud.  Prisma Cloud plays in several categories that overlap with GitLab and can be positioned as both a partner and a competitor.  [Pricing](https://aws.amazon.com/marketplace/pp/B07QDBGS6L?qid=1591116481430&sr=0-1&ref_=srh_res_product_title) is based on the number of “workload” (aka. pods) that are protected.

## Software Composition Analysis

* GitLab [Direction](https://about.gitlab.com/direction/secure/composition-analysis/container-scanning/)
   - Strengths
      - Integrated security as part of DevOps workflow for all developers
      - High-quality container security by leveraging all the latest feeds for vulnerabilities
      - Security leadership by being a CVE Numbering Authority
      - End-to-end DevOps offering from SCM to CI to CD to Security and more
   - Gaps
      - Requires users to use GitLab for CI if they are not already

* Prisma Cloud [Details](https://www.paloaltonetworks.com/prisma/cloud)
   - Strenghts
      - Provides a basic analysis of installed packages vs. known CVEs
      - Nice, clean UX and design
   - Gaps
      - Does not provide a full suite of code scanning to adequately detect all vulnerabilities - no SAST or DAST
      - Shifting left and integrating with SCM tools requires an integration to be built with 
         their APIs and does not exist natively

## Vulnerability Management

* GitLab
   - Strenghts
      - One tool - vulnerability management is integrated out-of-the-box
      - Visualizes data from all of GitLab’s scanning engines, including DAST, SAST, and SCA
   - Gaps
      - Current functionality is new and lacks features
      - Risk assessment capabilities do not exist and priorities do not take into context the 
         configuration of the  container, which could mitigate some vulnerabilities

* Prisma Cloud
   - Strenghts
      - Capable of enforcing policy rules and preventing vulnerable code from running
      - Good visualization of a complex risk analysis
      - Capable of showing CVE severity together with the container’s actual exposure to the specific attack
   - Gaps
      - Lacks good scanners to identify vulnerabilities
      - Not natively integrated with SCM tools - an API is available but the integration has to be built into CI/CD
      - Limited to containerized applications

## Container Security

* GitLab [Direction](https://about.gitlab.com/direction/defend/container_host_security/)
   - Strengths
      - ICurrent functionality provides a respectable baseline of security (Web application firewall, 
        container Network Policies, container host monitoring)
      - All capabilities are currently available in core (Note: future capabilities will likely be 
         added in a paid tier)
   - Gaps
      - Ability to block/prevent activity is currently limited
      - Although the GitLab roadmap is robust, as of today there are several large  feature/functionality gaps

* Prisma Cloud [Details](https://www.paloaltonetworks.com/prisma/cloud/compute-security)
   - Strenghts
      - Extensive set of features and capabilities
      - “Radar” capability gives a wow factor in visualizing the network
      - Protection for serverless code

   - Gaps
      - Pricing model can get expensive fast with lots of containers
      - Behind the nice UX, the solution is hard to manage.  One Prisma Cloud user reported 
        needing a team of 4-5 to operate it running in production.



## How to position GitLab

Prisma Cloud is a decent choice if the customer only needs basic vulnerability scanning; however, to properly secure their applications, they should consider a solution that includes good SAST and DAST scanners.  Rather than using separate scanners to meet their needs, it will be much simpler and easier to use GitLab, which both has a wide range of scanning capabilities as well as a native integration with SCM. Additionally, GitLab is a Niche player in the Gartner Magic Quadrant for AST.

When positioning as a competitor, highlight that their vulnerability management tool only intakes data from a very limited source: known CVEs.  This leaves them blind to other vulnerabilities that may be identified through SAST or DAST scans.  Rather than integrating Prisma Cloud as a separate product into CI/CD pipeline jobs, it will be much easier for customers to just use the built-in vulnerability management capabilities of GitLab that come available out-of-the-box.

Prisma Cloud can be positioned as a partner with GitLab, as it is possible to feed their scan results into GitLab and combine them with the results from other GitLab scans.

Prisma Cloud is expensive, while much of the current feature set that GitLab provides is available for free.  Additionally, the heavy operational maintenance burden of Prisma Cloud further adds to the cost.  If what GitLab provides today can be considered ‘good enough’, then customers can potentially save a huge amount of money.

## Comparison
